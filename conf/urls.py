from django.conf.urls import patterns, include, url
from content.views import (
    Index,
    StaticPage,
    NewsList,
    NewsView,
    ArticlesList,
    ArticleView,
    Custom404)
from search.views import Search
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.conf import settings
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^ulogin/', include('django_ulogin.urls')),
    url(r'^news/$', NewsList.as_view(), name='news_list'),
    url(r'^news/(?P<identity>.*)/$', NewsView.as_view(), name='news_view'),
    url(r'^articles/$', ArticlesList.as_view(), name='articles_list'),
    url(r'^articles/category/(?P<identity>.*)/$', ArticlesList.as_view(), name='articles_category'),
    url(r'^articles/(?P<identity>.*)/$', ArticleView.as_view(), name='article_view'),
    url(r'^advert/', include('advert.urls', namespace='advert')),
    url(r'^catalog/', include('catalog.urls', namespace='catalog')),
    url(r'^auth/', include('auth.urls', namespace='auth')),
    url(r'^user/', include('auth.urls', namespace='user')),
    url(r'^404/$', Custom404.as_view(), name='404'),
    url(r'^$', Index.as_view(), name='index'),
    url(r'^search/$', Search.as_view(), name='search'),

    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^admin/?', include(admin.site.urls)),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'(?P<slug>.*)/?$', StaticPage.as_view(), name='static_page'),

)

handler404 = 'content.views.handler404'

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()


# -*- coding: utf-8 -*-
import requests
from django.views.generic import TemplateView
import requesocks
from pyquery import PyQuery as pq
import subprocess
import time
from stem import Signal
from stem.control import Controller
import urlparse
from common.shortcuts import findstr

class Search(TemplateView):
    base_url = 'https://www.google.ru/search?hl=ru&source=hp&q='
    template_name = 'search/results.html'

    def get_context_data(self):
        data = self.__do_search_request()

        res = []
        if data:
            for li in pq(data).find('li.g'):
                li = pq(li)
                a = li.find('h3 a')
                res.append({
                    'url': self.__parse_url(a.attr('href')),
                    'title': a.text(),
                    'content': li.find('span.st').html()
                })
        return {'results': res}

    def __do_search_request(self):
        data = ''
        for i in range(0, 3):
            with Controller.from_port(port=8118) as controller:
                controller.authenticate()
                controller.signal(Signal.NEWNYM)
                time.sleep(1)

            try:
                s = requesocks.session()
                s.proxies = {
                    'https': 'socks5://127.0.0.1:9050',
                    'http': 'socks5://127.0.0.1:9050'
                }

                query = self.request.GET.get('q')
                r = s.get(
                    self.base_url + query + ' site:askremont.ru',
                    headers={'Referer': 'https://www.google.ru/'}
                )
                if r.status_code == 200:
                    data = r.text
                    break
            except:
                pass
        return data

    def __parse_url(self, s):
        qs = findstr(s, '/url?', '&sa=')
        try:
            res = urlparse.parse_qs(qs)
            return res['q'][0]
        except:
            return '/'




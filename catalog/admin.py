# -*- coding: utf-8 -*-
from django.conf import settings

from django.contrib import admin
from mptt.admin import MPTTModelAdmin

from catalog.models import City, Company, Category, Service, WorkSample, Tag
from common.shortcuts import send_mail
from seo.models import Seo

class CityAdmin(admin.ModelAdmin):
    pass
admin.site.register(City, CityAdmin)

class TagAdmin(admin.ModelAdmin):
    pass
admin.site.register(Tag, TagAdmin)

class SeoInline(admin.StackedInline):
    extra=0
    max_num = 1
    model=Seo
    fields = ('title', 'description', 'keywords', 'visible_text')

class CategoryAdmin(MPTTModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    inlines = (SeoInline,)
    list_per_page = 400
admin.site.register(Category, CategoryAdmin)


class ServiceInline(admin.TabularInline):
    extra = 0
    model = Service


class WorkSampleInline(admin.TabularInline):
    extra = 0
    model = WorkSample


class CompanyAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    inlines = (ServiceInline, WorkSampleInline,SeoInline,)

    def save_model(self, request, obj, form, change):
        if 'status' in form.changed_data:
            s = obj.status
            if s == 'active':
                txt = u'''
                Ваша компания упешно прошла модерацию на askremont.ru<br>
                <a href="http://askremont.ru%s">Посмотреть компанию на сайте</a>
                ''' % obj.link
                send_mail(
                    u'Ваша компания упешно прошла моделацию на askremont.ru',
                    txt,
                    settings.EMAIL_HOST_USER,
                    [obj.email],
                )


        if not change:
            obj.user = request.user

        super(CompanyAdmin, self).save_model(request, obj, form, change)
admin.site.register(Company, CompanyAdmin)


# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from catalog.views import (
    CompanyView,
    CategoryView,
    TagView,
    UploadFile,
    CreateCompanyView,
    UpdateCompanyView,
    JsTreeCategories
)

urlpatterns = patterns('catalog.views',

    url(r'^company/edit/(?P<pk>[0-9]+)/$', UpdateCompanyView.as_view(), name='company_edit'),
    url(r'^company/new/$', CreateCompanyView.as_view(), name='company_add'),
    url(r'^company/upload_file/$', UploadFile.as_view(), name='company_upload_file'),
    url(r'^company/(?P<identity>.*)/$', CompanyView.as_view(), name='company_view'),
    url(r'^tag/(?P<tag>.*)/$', TagView.as_view(), name='tag_view'),
    url(r'^get_categories/(?P<company_id>[0-9]+)/$', JsTreeCategories.as_view(), name='get_categories'),
    url(r'^(?P<identity>.*)/$', CategoryView.as_view(), name='category'),
)


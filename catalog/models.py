# -*- coding: utf-8 -*-
import random
from django.core.urlresolvers import reverse
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey, TreeManyToManyField
from common.shortcuts import upload_path_constructor
from common.models import BaseModel, CommonModelMixins
from django.contrib.auth.models import User
from django.core.validators import MaxLengthValidator


class City(models.Model):
    title = models.CharField(
        verbose_name=u'Название',
        max_length=255,
    )

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Город'
        verbose_name_plural = u'Города'


class Tag(BaseModel):
    '''
    Теги компаний
    '''
    title = models.CharField(
        verbose_name = u'Название',
        max_length=255,
    )

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Тег'
        verbose_name_plural = u'Теги'


class Category(MPTTModel, CommonModelMixins):
    '''
    Категория компаний
    '''
    title = models.CharField(
        verbose_name=u'Название',
        max_length=255,
    )
    slug = models.SlugField(
        verbose_name=u'SEO URL',
        blank=True, null=True,
    )
    parent = TreeForeignKey(
        'self',
        verbose_name=u'Родительская категория',
        related_name='children',
        blank=True, null=True,
    )
    description = models.TextField(
        verbose_name=u'Краткое описание',
        help_text=u'Обязательно если категория будет показываться на главной',
        null=True, blank=True,
    )
    sort_pos = models.IntegerField(
        verbose_name=u'Порядок сортировки',
        default=0,
    )
    show_on_main = models.BooleanField(
        verbose_name=u'Показывать на главной',
        default=False
    )
    promo_block = models.TextField(
        verbose_name=u'Боковой промо блок',
        blank=True, null=True,
    )


    @property
    def link(self):
        return reverse('catalog:category', kwargs={'identity': self.slug or self.pk})


    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Категория'
        verbose_name_plural = u'Категории'

    class MPTTMeta:
        order_insertion_by = ['title']


COMPANY_STATUS_CHOICES = (
    ('active', u'Активная'),
    ('off', u'Отключена'),
    ('wantscheck', u'Требует проверки')
)


class Company(BaseModel):
    title = models.CharField(
        verbose_name=u'Название',
        max_length=255,
    )
    slug = models.SlugField(
        verbose_name=u'SEO URL',
        blank=True, null=True,
    )
    logo = models.ImageField(
        upload_to=upload_path_constructor,
        verbose_name=u'Логотип',
        null=True, blank=True
    )
    owner = models.ForeignKey(
        User,
        verbose_name=u'Владелец',
        null=True, blank=True,
    )
    description = models.TextField(
        verbose_name=u'Полное описание',
        blank=True, null=True,
    )
    slogan = models.TextField(
        verbose_name=u'Слоган',
        blank=True, null=True,
    )
    categories = TreeManyToManyField(
        Category,
        verbose_name=u'Сферы деятельности',
    )
    city = models.ForeignKey(
        City,
        verbose_name=u'Город',
        null=True, blank=True,
    )
    address = models.TextField(
        verbose_name=u'Адрес',
        null=True, blank=True,
    )
    site = models.CharField(
        max_length=200,
        verbose_name=u'Сайт',
        null=True, blank=True,
    )
    email = models.CharField(
        max_length=200,
        verbose_name=u'E-mail'
    )
    phones = models.CharField(
        max_length=200,
        verbose_name=u'Телефоны',
        null=True, blank=True,
    )
    contact_person = models.CharField(
        max_length=200,
        verbose_name=u'Контактное лицо',
        null=True, blank=True,
    )
    status = models.CharField(
        verbose_name=u'Статус',
        max_length=20,
        choices=COMPANY_STATUS_CHOICES,
        default='active',
    )
    price_min = models.IntegerField(
        verbose_name=u'Минимальная цена услуг',
        null=True, blank=True,
    )
    price_max = models.IntegerField(
        verbose_name=u'Максимальная цена услуг',
        null=True, blank=True,
    )
    show_on_main = models.BooleanField(
        verbose_name=u'Показывать на главной',
        default=False,
    )
    tags = models.ManyToManyField(
        Tag,
        verbose_name=u'Теги',
        null=True, blank=True,
    )

    @property
    def prices_range(self):
        if self.price_min and self.price_max:
            return u'%s - %s' % (self.price_min, self.price_max)
        if self.price_min:
            return u'от %s' % self.price_min
        if self.price_max:
            return u'до %s' % self.price_max
        return u''

    def __unicode__(self):
        return self.title

    @property
    def link(self):
        if self.slug and self.slug.strip():
            slug = self.slug.strip()
        else:
            slug = self.pk
        return u'/catalog/company/%s/' % slug

    def get_absolute_url(self):
        return self.link

    def random_worksets(self):
        sets = list(self.worksample_set.all())
        random.shuffle(sets)
        return sets[0:4]

    class Meta:
        verbose_name = u'Компания'
        verbose_name_plural = u'Компании'


class Service(BaseModel):
    company = models.ForeignKey(
        Company,
        verbose_name=u'Компания',
    )
    title = models.CharField(
        verbose_name=u'Название',
        max_length=255,
    )
    description = models.TextField(
        verbose_name=u'Полное описание',
        validators=[MaxLengthValidator(80)],
        blank=True, null=True,
    )
    cost_type = models.CharField(
        verbose_name=u'Тип стоимости',
        default='individual',
        max_length=50,
        choices=(
            ('individual', u'Индивидуальный расчет'),
            ('fixed', u'Фиксированная стоимость')
        )
    )
    cost = models.IntegerField(
        verbose_name=u'Стоимость',
        default=None,
        null=True, blank=True,
    )

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Услуга компании'
        verbose_name_plural = u'Услуги компаний'


class WorkSample(BaseModel):
    company = models.ForeignKey(
        Company,
        verbose_name=u'Компания',
    )
    image = models.ImageField(
        upload_to=upload_path_constructor,
        verbose_name=u'Фото',
        null=True, blank=True
    )
    cost = models.CharField(
        verbose_name=u'Стоимость',
        max_length=255,
        default=None,
        null=True, blank=True,
    )
    description = models.TextField(
        verbose_name=u'Краткое описание',
    )

    def __unicode__(self):
        return self.description

    class Meta:
        verbose_name = u'Пример работы компании'
        verbose_name_plural = u'Примеры работ компаний'

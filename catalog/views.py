# -*- coding: utf-8 -*-
from django.forms.models import modelformset_factory
from django.http import Http404
from digg_paginator import DiggPaginator
from django.db.models import Q

from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView, ListView
from extra_views.advanced import CreateWithInlinesView
from catalog.forms import CompanyForm, ServiceForm, WorkSampleForm
from common.shortcuts import force_int, get_object_or_none
from common.views.generic import CommonAjaxUploadView, LoginRequiredMixin, JsonView
from content.models import Content
from catalog.models import Company, Category, Tag, Service, WorkSample
from extra_views import InlineFormSet, UpdateWithInlinesView


class CompanyView(TemplateView):
    '''
    Просмотр компании
    '''
    template_name = 'catalog/company_view.html'
    ctx_name = 'company'

    def get_context_data(self, **kwargs):
        ctx = super(CompanyView, self).get_context_data(**kwargs)
        nid = self.kwargs.get('identity')
        pk_filter = Q(slug=nid)
        try:
            pk_filter = Q(pk=int(nid))
        except:
            pass

        status_filter = Q(status='active')
        u = self.request.user
        if u.is_authenticated():
            status_filter |= Q(Q(status='wantscheck') & Q(owner=u))

        try:
            company = Company.objects.get(
                pk_filter,
                status_filter
            )
        except Company.DoesNotExist:
            raise Http404(u'Компания не найдена')


        #company.inc('views_count')

        ctx.update({
            self.ctx_name: company,
            'seo_data': company.seo_data,
            'vertical': 'catalog'
        })
        return ctx


def shared_queryset_modifier(qs, request):
        order = request.GET.get('price_order', 'desc')
        if order in ['asc', 'desc']:
            qs = qs.order_by('price_min' if order == 'asc' else '-price_min')

        price_min = force_int(request.GET.get('price_min', '0'))
        if price_min:
            qs = qs.filter(price_min__gte=price_min)

        price_max = force_int(request.GET.get('price_max', '0'))
        if price_max and price_max >= price_min:
            qs = qs.filter(price_max__lte=price_max)
        return qs


class CategoryView(ListView):
    template_name = 'catalog/subcategory.html'
    context_object_name = 'companies'
    model = Company
    paginator_class = DiggPaginator
    paginate_by = 5

    def get_queryset(self):
        category = Category.get_by_identity(self.kwargs.get('identity'))
        self.category = category
        self.subcategories = list(category.get_children())
        qs = super(CategoryView, self).get_queryset()
        qs = qs.prefetch_related('worksample_set').filter(categories__in=[category])
        return shared_queryset_modifier(qs, self.request)

    def get_context_data(self, **kwargs):
        ctx = super(CategoryView, self).get_context_data(**kwargs)
        ctx['category'] = self.category
        if self.subcategories:
            ctx['subcategories'] = self.subcategories
            self.template_name = 'catalog/category.html'
        ctx['seo_data'] = self.category.seo_data
        ctx['vertical'] = 'catalog'
        ctx['tag_name'] = ''
        return ctx


class TagView(ListView):
    template_name = 'catalog/subcategory.html'
    context_object_name = 'companies'
    paginator_class = DiggPaginator
    paginate_by = 5
    model = Tag

    def get_queryset(self):
        ctx = super(TagView, self).get_queryset()
        tag_name = self.kwargs.get('tag')
        self.tag = get_object_or_404(Tag, title=tag_name)
        qs = self.tag.company_set.prefetch_related('worksample_set', 'categories')
        return shared_queryset_modifier(qs, self.request)

    def get_context_data(self, **kwargs):
        ctx = super(TagView, self).get_context_data(**kwargs)
        tag_name = self.kwargs.get('tag')
        ctx['seo_data'] = self.tag.seo_data
        ctx['vertical'] = 'catalog'
        ctx['tag_name'] = tag_name
        return ctx


class ServiceInline(InlineFormSet):
    model = Service
    extra = 1


class WorkSampleInline(InlineFormSet):
    model = WorkSample
    extra = 1
    form_class = WorkSampleForm


class CreateCompanyView(CreateWithInlinesView):
    template_name = 'catalog/company_edit.html'
    model = Company
    form_class = CompanyForm

    def get_inlines(self):
        s = ServiceInline
        s.extra = 2
        w = WorkSampleInline
        w.extra = 2
        return [s, w]

    def get_form_class(self):
        cls = CompanyForm
        cls.user = self.request.user
        return cls


class UpdateCompanyView(UpdateWithInlinesView):
    template_name = 'catalog/company_edit.html'
    model = Company
    inlines = [ServiceInline, WorkSampleInline]

    def get_form_class(self):
        cls = CompanyForm
        cls.user = self.request.user
        return cls


class UploadFile(LoginRequiredMixin, CommonAjaxUploadView):
    model_name = 'company_images'
    thumb_size = '200x200'

    def preprocess(self):
        self.thumb_size = self.request.POST.get('thumb_size') or self.thumb_size
        ret = super(UploadFile, self).preprocess()
        fname = ret.get('fname')
        if fname:
            ret['fname'] = fname
        return ret

    def get_rel_path(self):
        return self.model_name

    def apply_to_model(self, fname):
        return True

from mptt.templatetags.mptt_tags import cache_tree_children

class JsTreeCategories(JsonView):
    def preprocess(self):
        ret = []
        data = list(Category.objects.filter(parent__isnull=True))

        self.selected_ids = [];
        company = get_object_or_none(Company, pk=self.kwargs.get('company_id'))
        if company:
            self.selected_ids = company.categories.values_list('pk', flat=True)

        for c in data:
            ret.append(
                self.recursive_node_to_dict(c)
            )

        return ret

    def recursive_node_to_dict(self, node):
        result = {
            'id': node.pk,
            'label': node.title,
            'checkbox': False,
        }
        children = [self.recursive_node_to_dict(c) for c in node.get_children()]
        if children:
            result['branch'] = children
        else:
            result['checkbox'] = True
            if node.pk in self.selected_ids:
                result['checked'] = True
        return result
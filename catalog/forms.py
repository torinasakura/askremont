# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from catalog.models import Company, Service, WorkSample
from common.shortcuts import send_mail


class CompanyForm(forms.ModelForm):
    logo = forms.CharField(widget=forms.HiddenInput())
    owner = forms.CharField(widget=forms.HiddenInput(), required=False)
    address = forms.CharField()
    status = forms.CharField(widget=forms.HiddenInput(), required=False)

    def clean_owner(self):
        return self.user

    def clean_status(self):
        return 'wantscheck'

    def save(self, commit=True):
        item = super(CompanyForm, self).save(commit)
        action = self.data.get('domoderate')
        if commit and len(action) and action[0]:
            txt = '''
            <a href="http://askremont.ru/admin/catalog/company/%s/">Перейти к модерации</a>
            ''' % item.pk

            send_mail(
                u'Компания требует модерации',
                txt,
                settings.EMAIL_HOST_USER,
                settings.CATALOGUE_MODERATORS
            )
        return item

    def clean_categories(self):
        cats = self.cleaned_data.get('categories')
        clist = list(cats)
        for c in clist:
            if c.get_descendant_count() > 0:
                raise ValidationError(u'Выбрана родительская категория')
        if len(clist) > 3:
            raise ValidationError(u'Выбрано больше трех категорий')
        return cats


    class Meta:
        model = Company
        exclude = [
            'price_min',
            'price_max',
            'show_on_main',
            'slug'
        ]


class ServiceForm(forms.ModelForm):
    class Meta:
        model = Service


class WorkSampleForm(forms.ModelForm):
    image = forms.CharField(widget=forms.HiddenInput())
    class Meta:
        model = WorkSample
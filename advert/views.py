# -*- coding: utf-8 -*-
# Create your views here.
import datetime
from django.shortcuts import get_object_or_404
from django.views.generic import RedirectView
from advert.models import Banner, BannerTransition
from common.shortcuts import get_object_or_none


class Transition(RedirectView):
    def get_redirect_url(self, **kwargs):
        banner = get_object_or_404(Banner, status='active', pk=self.kwargs.get('banner_id'))

        bt = get_object_or_none(BannerTransition, banner=banner, date=datetime.datetime.now())

        if not bt:
            bt = BannerTransition(
                banner=banner,
                date=datetime.datetime.now()
            )
            bt.save()

        bt.inc('count')

        return banner.link

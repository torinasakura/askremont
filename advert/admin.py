# -*- coding: utf-8 -*-

from django.contrib import admin
from advert.models import Section, Banner, Format



class FormatAdmin(admin.ModelAdmin):
    pass
admin.site.register(Format, FormatAdmin)

class SectionAdmin(admin.ModelAdmin):
    pass
admin.site.register(Section, SectionAdmin)


class BannerAdmin(admin.ModelAdmin):
    pass
admin.site.register(Banner, BannerAdmin)
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from advert.views import Transition

urlpatterns = patterns('advert.views',
    url(r'^transition/(?P<banner_id>[0-9])/$', Transition.as_view(), name='transition'),
)

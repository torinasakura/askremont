# -*- coding: utf-8 -*-
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django import template
from advert.models import Banner
register = template.Library()


@register.simple_tag(takes_context=True)
def advert(context, vertical, section, template='default'):
    '''
    Тег для отображения баннера из заданной секции
    '''
    banner, section = Banner.show(vertical, section)

    data = {
        'banner': banner,
        'stub': section.format.stub if section else None,
        'stub_size': '%sx%s' % (section.format.width, section.format.height) if section else None
    }
    print vertical, section, data

    ret = render_to_string('advert/%s.html' % template, data, context_instance=context)
    return mark_safe(ret)

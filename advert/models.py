# -*- coding: utf-8 -*-

import datetime
import re
from django.db import models
from common.models import BaseModel
from common.shortcuts import get_object_or_none, upload_path_constructor


class Format(BaseModel):
    title = models.CharField(
        verbose_name=u'Название',
        max_length=255,
    )

    width = models.PositiveIntegerField(
        verbose_name=u'Ширина',
    )
    height = models.PositiveIntegerField(
        verbose_name=u'Высота',
    )
    stub = models.ImageField(
        upload_to=upload_path_constructor,
        verbose_name=u'Картинка-заглушка',
        max_length=255
    )

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Формат банера'
        verbose_name_plural = u'Формат банера'

class Section(BaseModel):
    '''
    Баннерные секции
    '''
    title = models.CharField(
        verbose_name=u'Название',
        max_length=255,
    )
    tech_name = models.CharField(
        verbose_name=u'Техническое имя',
        max_length=255,
        unique=True
    )

    format = models.ForeignKey(
        Format,
        verbose_name=u'Формат'
    )

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Баннерная секция'
        verbose_name_plural = u'Баннерные секции'


BANNER_STATUS_CHOICES = (
    ('active', u'В ротации'),
    ('draft', u'Черновик'),
    ('disabled', u'Снят с ротации'),
)

class Banner(BaseModel):
    '''
    Баннер
    '''
    title = models.CharField(
        verbose_name=u'Название',
        max_length=255
    )
    link = models.CharField(
        verbose_name=u'Ссылка',
        max_length=255,
        null=True, blank=True
    )
    status = models.CharField(
        verbose_name=u'Статус',
        max_length=50,
        choices=BANNER_STATUS_CHOICES,
        default='active',
    )
    date_from = models.DateField(
        verbose_name=u'Начало ротации',
    )
    date_to = models.DateField(
        verbose_name=u'Завершение ротации',
    )
    file_name = models.FileField(
        upload_to=upload_path_constructor,
        verbose_name=u'Файл баннера',
        max_length=255,
    )
    sections = models.ManyToManyField(
        Section,
        verbose_name=u'Секции',
    )

    @property
    def type(self):
        return 'flash' if self.src.endswith('.swf') else 'image'

    @property
    def src(self):
        return self.file_name

    def __unicode__(self):
        return self.title

    @classmethod
    def _get_vertical_names(cls):
        return ['index', 'news', 'catalog', 'articles', 'static']

    @classmethod
    def show(cls, vertical, section_name, last_pass=False):
        '''
        Возвращает баннер для показа в заданной вертикали и с нужным названием секции
        и регистрирует его показ
        '''
        section = False
        if vertical and not last_pass:
            section_name = ('%s_%s' % (vertical, section_name)).strip('_')

        section = None
        try:
            section = Section.objects.select_related('format').get(tech_name=section_name)
        except Section.DoesNotExist:
            pass

        if section:
            now = datetime.datetime.now()
            banners = list(section.banner_set.filter(
                status='active',
                date_from__lte=now,
                date_to__gte=now
            ).order_by('?')[0:1])

            if banners:
                banner = banners[0]
                bsh = get_object_or_none(BannerShow, date=datetime.datetime.now(), banner=banner)
                if not bsh:
                    bsh = BannerShow(
                        date=datetime.datetime.now(),
                        banner=banner
                    )
                    bsh.save()
                bsh.inc('count')
                return banner, section
            else:
                return None, section

        if last_pass:
            return None, section

        # Попытка повторного выбора секции неспецифичной для вертикали
        vertical_names = cls._get_vertical_names()
        pattern = r'^(' + '|'.join(vertical_names) + r')_'
        new_section_name = re.sub(pattern, '', section_name)

        if section_name != new_section_name:
            return cls.show(None, new_section_name, True)
        return None, section

    class Meta:
        verbose_name = u'Баннер'
        verbose_name_plural = u'Баннеры'


class BannerShow(BaseModel):
    '''
    Показы баннеров - сгруппированы по дням
    '''
    banner = models.ForeignKey(Banner)
    date = models.DateField(auto_now_add=True)
    count = models.PositiveIntegerField(
        default=0
    )
    class Meta:
        verbose_name = u'Показ'
        verbose_name = u'Показы'


class BannerTransition(BaseModel):
    '''
    Переходы после кликов по баннеру - сгруппированы по дням
    '''
    banner = models.ForeignKey(Banner)
    date = models.DateField()
    count = models.PositiveIntegerField(
        default=0
    )
    class Meta:
        verbose_name = u'Клик'
        verbose_name = u'Клики'
# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Section'
        db.create_table(u'advert_section', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('tech_name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
        ))
        db.send_create_signal(u'advert', ['Section'])

        # Adding model 'Banner'
        db.create_table(u'advert_banner', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('link', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('status', self.gf('django.db.models.fields.CharField')(default='active', max_length=50)),
            ('date_from', self.gf('django.db.models.fields.DateField')()),
            ('date_to', self.gf('django.db.models.fields.DateField')()),
            ('file_name', self.gf('django.db.models.fields.files.FileField')(max_length=255)),
            ('width', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('height', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'advert', ['Banner'])

        # Adding M2M table for field sections on 'Banner'
        m2m_table_name = db.shorten_name(u'advert_banner_sections')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('banner', models.ForeignKey(orm[u'advert.banner'], null=False)),
            ('section', models.ForeignKey(orm[u'advert.section'], null=False))
        ))
        db.create_unique(m2m_table_name, ['banner_id', 'section_id'])

        # Adding model 'BannerShow'
        db.create_table(u'advert_bannershow', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('banner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['advert.Banner'])),
            ('date', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
            ('count', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal(u'advert', ['BannerShow'])

        # Adding model 'BannerTransition'
        db.create_table(u'advert_bannertransition', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('banner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['advert.Banner'])),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('count', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal(u'advert', ['BannerTransition'])


    def backwards(self, orm):
        # Deleting model 'Section'
        db.delete_table(u'advert_section')

        # Deleting model 'Banner'
        db.delete_table(u'advert_banner')

        # Removing M2M table for field sections on 'Banner'
        db.delete_table(db.shorten_name(u'advert_banner_sections'))

        # Deleting model 'BannerShow'
        db.delete_table(u'advert_bannershow')

        # Deleting model 'BannerTransition'
        db.delete_table(u'advert_bannertransition')


    models = {
        u'advert.banner': {
            'Meta': {'object_name': 'Banner'},
            'date_from': ('django.db.models.fields.DateField', [], {}),
            'date_to': ('django.db.models.fields.DateField', [], {}),
            'file_name': ('django.db.models.fields.files.FileField', [], {'max_length': '255'}),
            'height': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'sections': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['advert.Section']", 'symmetrical': 'False'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'active'", 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'width': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'advert.bannershow': {
            'Meta': {'object_name': 'BannerShow'},
            'banner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['advert.Banner']"}),
            'count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'date': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'advert.bannertransition': {
            'Meta': {'object_name': 'BannerTransition'},
            'banner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['advert.Banner']"}),
            'count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'advert.section': {
            'Meta': {'object_name': 'Section'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tech_name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['advert']
# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Format'
        db.create_table(u'advert_format', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('width', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('height', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('stub', self.gf('django.db.models.fields.files.ImageField')(max_length=255)),
        ))
        db.send_create_signal(u'advert', ['Format'])

        # Deleting field 'Section.height'
        db.delete_column(u'advert_section', 'height')

        # Deleting field 'Section.stub'
        db.delete_column(u'advert_section', 'stub')

        # Deleting field 'Section.width'
        db.delete_column(u'advert_section', 'width')


    def backwards(self, orm):
        # Deleting model 'Format'
        db.delete_table(u'advert_format')


        # User chose to not deal with backwards NULL issues for 'Section.height'
        raise RuntimeError("Cannot reverse this migration. 'Section.height' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Section.height'
        db.add_column(u'advert_section', 'height',
                      self.gf('django.db.models.fields.PositiveIntegerField')(),
                      keep_default=False)

        # Adding field 'Section.stub'
        db.add_column(u'advert_section', 'stub',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=255, null=True, blank=True),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Section.width'
        raise RuntimeError("Cannot reverse this migration. 'Section.width' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Section.width'
        db.add_column(u'advert_section', 'width',
                      self.gf('django.db.models.fields.PositiveIntegerField')(),
                      keep_default=False)


    models = {
        u'advert.banner': {
            'Meta': {'object_name': 'Banner'},
            'date_from': ('django.db.models.fields.DateField', [], {}),
            'date_to': ('django.db.models.fields.DateField', [], {}),
            'file_name': ('django.db.models.fields.files.FileField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'sections': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['advert.Section']", 'symmetrical': 'False'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'active'", 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'advert.bannershow': {
            'Meta': {'object_name': 'BannerShow'},
            'banner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['advert.Banner']"}),
            'count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'date': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'advert.bannertransition': {
            'Meta': {'object_name': 'BannerTransition'},
            'banner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['advert.Banner']"}),
            'count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'advert.format': {
            'Meta': {'object_name': 'Format'},
            'height': ('django.db.models.fields.PositiveIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'stub': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'width': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'advert.section': {
            'Meta': {'object_name': 'Section'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tech_name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['advert']
# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Banner.height'
        db.delete_column(u'advert_banner', 'height')

        # Deleting field 'Banner.width'
        db.delete_column(u'advert_banner', 'width')

        # Adding field 'Section.width'
        db.add_column(u'advert_section', 'width',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Section.height'
        db.add_column(u'advert_section', 'height',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Section.stub'
        db.add_column(u'advert_section', 'stub',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=255, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Banner.height'
        db.add_column(u'advert_banner', 'height',
                      self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Banner.width'
        db.add_column(u'advert_banner', 'width',
                      self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Section.width'
        db.delete_column(u'advert_section', 'width')

        # Deleting field 'Section.height'
        db.delete_column(u'advert_section', 'height')

        # Deleting field 'Section.stub'
        db.delete_column(u'advert_section', 'stub')


    models = {
        u'advert.banner': {
            'Meta': {'object_name': 'Banner'},
            'date_from': ('django.db.models.fields.DateField', [], {}),
            'date_to': ('django.db.models.fields.DateField', [], {}),
            'file_name': ('django.db.models.fields.files.FileField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'sections': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['advert.Section']", 'symmetrical': 'False'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'active'", 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'advert.bannershow': {
            'Meta': {'object_name': 'BannerShow'},
            'banner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['advert.Banner']"}),
            'count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'date': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'advert.bannertransition': {
            'Meta': {'object_name': 'BannerTransition'},
            'banner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['advert.Banner']"}),
            'count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'advert.section': {
            'Meta': {'object_name': 'Section'},
            'height': ('django.db.models.fields.PositiveIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'stub': ('django.db.models.fields.files.ImageField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'tech_name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'width': ('django.db.models.fields.PositiveIntegerField', [], {})
        }
    }

    complete_apps = ['advert']
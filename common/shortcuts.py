# -*- coding: utf8 -*-

import os
import re
import pytils

from urlobject import URLObject

from django.core.urlresolvers import Resolver404, resolve
from django.shortcuts import redirect
from django.contrib import messages
from django.conf import settings
from django.db.models.loading import get_model
from django.core.mail import get_connection
from django.core.mail.message import EmailMessage


def redirect_with_message(request, to, message, **kwargs):
    '''
    Делает редирект на заданную страницу с прикреплением
    текстового сообщения через messages framework
    '''
    if message:
        messages.add_message(request, messages.ERROR, message)
    return redirect(to, **kwargs)


def get_referer(request, default='/'):
    '''
    Возвращает реферер не ссылающийся на текущую страницу
    '''
    original_referer = request.META.get('HTTP_REFERER')

    if not original_referer:
        return default

    url = URLObject(original_referer)

    # Проверяем нашего ли сайта домен
    host = '.%s' % url.hostname
    if url and not host.endswith('.%s' % settings.LOCAL.get('domain')):
        return default

    current_url = URLObject(request.get_full_path())

    # Проверяем не с этой же ли самой страницы реферер
    if url.path.strip('/') == current_url.path.strip('/'):
        return default
    return original_referer


def dict_path(input_dict, path, default=None):
    '''
    Получает значение вложенных словарей, объектов и списков по пути состоящему из ключей разделенных точками
    Например для словаря d = {'some':{'deep':[7, 6, {'nested': 'value'}]}}
    При вызове dict_path(d, 'some.deep.2.nested') вернет 'value'
    2 - здесь индекс элемента в списке [7, 6, {'nested': 'value'}]
    '''

    path = path.split('.')
    val = input_dict
    for k in path:
        try:
            val = val[int(k)] if k.isdigit() else val[k]
        except (KeyError, IndexError, TypeError):
            try:
                fn = getattr(val, k)
                val = fn() if callable(fn) else fn
            except AttributeError:
                return default
    return val


def model_from_object_name(object_name):
    '''
    Возвращает модель по ее названию
    Например для 'contest_member' вернет contest.models.Member
    '''
    parts = object_name.split('_', 1)
    app_name = object_name = parts[0]
    if len(parts) > 1:
        object_name = parts[1]
    return get_model(app_name, object_name)


def get_object_or_none(model, *args, **kwargs):
    try:
        return model.objects.get(*args, **kwargs)
    except model.DoesNotExist:
        return None


def ensure_path(path):
    '''
    Проверяет есть ли в файловой системе директрия преданныя в path
    Если нет, то пытается создать эту директорию включая создание всех родительстких директорий
    Возвращает созданный путь
    '''

    # Проверяем не создана ли уже директория
    if os.path.exists(path):
        if not os.path.isdir(path):
            raise Exception('%s still exists but it is not a directory')
        else:
            return path

    os.makedirs(path)
    return path


def dict_extract(d, keys, default=None):
    '''
    Получает значения ключей из массива.
    Если ключ не существует в массиве, подставится значение по умолчанию.
    '''
    found = {}
    for key in keys:
        found[key] = d.get(key, default) if hasattr(d, 'get') else d
    return found


def dict_map(callback, d, keys=None):
    for key, val in d.items():
        if isinstance(val, dict):
            d[key] = dict_map(callback, d[key])
        else:
            if not isinstance(keys, dict) or key in keys:
                if hasattr(__builtin__, callback):
                    try:
                        d[key] = getattr(__builtin__, callback)(d.get(key, 0))
                    except (ValueError, TypeError):
                        d[key] = 0
    return d


def safe_resolve(url):
    '''
    Не выбрасывающий исключение резолв url в правило роутинга
    '''
    try:
        match = resolve(url)
    except Resolver404:
        return None
    return match


def force_int(input):
    try:
        return int(input)
    except ValueError, TypeError:
        return 0


def send_mail(subject, message, from_email, recipient_list,
               fail_silently=False, auth_user=None, auth_password=None,
               connection=None):
    '''
    Анвлог функции django.core.mail.send_mail
    Но отправляет письмо в формате html
    '''
    connection = connection or get_connection(username=auth_user,
                                              password=auth_password,
                                              fail_silently=fail_silently)
    msg = EmailMessage(subject, message, from_email, recipient_list,
                        connection=connection)
    msg.content_subtype = 'html'
    msg.send()

def upload_path_constructor(instance, filename):
    # Транслитерируем имя файла, убираем лишие символы
    fname = pytils.translit.translify(unicode(filename.lower()))
    fname = re.sub(r'[^a-z0-9\-.]', '', fname)

    # Разбиваем на имя файла и расширение и укорачиваем до разумных пределов
    parts = fname.split('.')
    ext = parts[-1][-5:]
    fname = parts[0][:20]

    # Директория которую мы вернем для upload_to
    path = os.path.join('uploads')

    # Абсолютный путь к директории куда нужно сложит файл (если директории нет - создем ее)
    abs_path = os.path.join(settings.MEDIA_ROOT, path)
    if not os.path.exists(abs_path):
        os.makedirs(abs_path)

    # Если в указанной директории уже есть файл с таким именем, инкрементируем имя файла по принципу
    # myfile.xls => myfile.2.xls
    # myfile.2.xls => myfile.3.xls
    if os.path.exists(os.path.join(abs_path, fname)):
        m = re.search(r'\.([0-9]+)$', fname)
        if m and len(m.groups()>0):
            version = int(m.groups()[0]) + 1
            new_fname = '%s.$s.$s' % (re.sub(r'\.[0-9]+$', '', fname), str(version), ext)
            return upload_path_constructor(instance, new_fname)
        else:
            fname = '%s.2.%s' % (fname, ext)

    return os.path.join(path, '%s.%s' % (fname, ext))

def findstr(instr, *args):
        '''
        Ищет подстроку между предпоследним и последними аргументами
        предварительно передвигаясь в нужную позицию в строке по алгоритму:
        1. Найти позицию вхождения первого аргумента
        2. Начиная с этой позиции найти позицию вхождения следующего аргумента
        ...
        Например для входящей строки '78575111738236751117' и аргументов '8','8', '75', '17'
        будет возвращено '11' (строка между '75', '17' но после того как два раза встретится '8')
        '''
        outstr = instr
        args = list(args)
        last_arg = args.pop()
        if args:
            for pattern in args:
                idx = outstr.find(pattern)
                if idx == -1:
                    return ''
                outstr = outstr[idx+len(pattern):]
        idx = outstr.find(last_arg)
        if idx == -1:
            return ''
        return outstr[:idx]
# -*- coding: utf-8 -*-
from django.conf import settings
from django import forms
from django.template.loader import render_to_string
import django_settings
from common.shortcuts import send_mail


class RecallForm(forms.Form):
    name = forms.CharField(max_length=50)
    phone = forms.CharField(max_length=50)

    def clean(self):
        cd = self.cleaned_data
        if 'name' in cd and 'phone' in cd:
            self.send_recall_email()

    def send_recall_email(self):
        send_mail(
            u'Просьба пользователя перезвонить с doctor-smile',
            render_to_string(
                'mails/recall.html',
                self.cleaned_data
            ),
            django_settings.get('support_email'),
            django_settings.get('recall_email').split(','),
            not settings.DEBUG
        )


# -*- coding: utf8 -*-
import os
import json
from time import time
from django.conf import settings

from django.http import HttpResponse
from django.views.generic import ListView
from django.contrib.auth.decorators import login_required
from sorl.thumbnail import get_thumbnail


class JsonView(ListView):
    '''
    Единая точка возвращающая Response в формате JSON
    Если необходимо использовать этот генерик без выполнения запрсов к БД
    переопределите метод preprocess и не вызывайте его суперфункцию
    например так:
    def preprocess(self):
        return {'my':'data'}
    '''

    success_result = {
        'state' : True,
        'msg'   : ''
    }
    fail_result = {
        'state' : False,
        'msg'   : u'Не удалось выполнить операцию'
    }

    def preprocess(self):
        '''
        Функция для обеспечения возможности фильтрации и преобразования данных
        из QuerySet, возвращаемое значение должно быть корректным для
        стандартного JSON сериализатора
        :return: Окончательный набор данных которые увидит пользователь
        '''
        return self.get_queryset()

    def dispatch(self, request, *args, **kwargs):
        '''
        Сериализует данные в JSON и отдает в браузер
        :param request:
        :param args:
        :param kwargs:
        :return: HttpResponse
        '''
        data = json.dumps(self.preprocess())
        resp = HttpResponse(data, content_type='application/json')
        resp['Expires'] = 'Mon, 1 Jan 2000 01:00:00 GMT'
        resp['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0'
        resp['Pragma'] = 'no-cache'
        return resp

    def get_fail_result(self, msg):
        '''
        Обертка для переменной ошибочного результата запроса.
        Дает возможность заменить текст сообщения об ошибке
        '''
        self.fail_result['msg'] = msg
        return self.fail_result


class LoginRequiredMixin(object):

    @classmethod
    def as_view(cls):
        return login_required(super(LoginRequiredMixin, cls).as_view())

class CommonAjaxUploadView(JsonView):
    '''
    Общая вьюшка Ajax-загрузка файлов
    '''
    model_name = ''
    thumb_size = ''
    form_field_name = 'file'

    def preprocess(self):
        data = {
            'state': False,
            'msg': u'Не удалось загрузить картинку.'
        }

        usr = self.request.user

        f = self.request.FILES[self.form_field_name]

        # Директория для записи загруженной картики
        parts = f.name.split('.')
        ext = parts[-1][-5:].lower()

        fname = '%s.%s' % (str(time()).replace('.', ''), ext)
        path = os.path.join(settings.MEDIA_ROOT, self.model_name)
        if not os.path.exists(path):
            os.makedirs(path)

        destination = open(os.path.join(path, fname), 'wb+')
        for chunk in f.chunks():
            destination.write(chunk)
        destination.close()

        res = self.apply_to_model(fname)
        if res is True:
            data['state'] = True
            data['msg'] = u''
            data['thumb'] = get_thumbnail(os.path.join(self.model_name, fname), self.thumb_size).url
            data['fname'] = os.path.join(self.model_name, fname)
        else:
            data['msg'] = u'%s %s' % (data.get('msg'), res)
        return data

    def apply_to_model(self, fname):
        raise NotImplementedError(u'Необходимо реализовать метод apply_to_model')
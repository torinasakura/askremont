# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from catalog.models import Category
from django.conf import settings

def catalog_categories(request):
    qs = Category.objects.filter(parent__isnull=True).order_by('sort_pos')
    return {
        'catalog_categories': qs
    }

def auth(request):
    u = request.user
    ret = {
        'domain': settings.DOMAIN
    }
    if not u.is_authenticated():
        return ret

    companies = list(u.company_set.all()[0:1])
    company = companies[0] if companies else None
    ret.update({
        'current_user': u,
        'current_company': company,
        'lk_url': __get_lk_url(u, company)
    })
    return ret

def __get_lk_url(u, company):
    if company:
        lk_url = reverse('catalog:company_edit', kwargs={'pk':company.pk})
    else:
        lk_url = reverse('catalog:company_add')
    return lk_url


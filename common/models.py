# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.
from django.db.models import Q
from django.http import Http404

class CommonModelMixins(object):
    def inc(self, field, by=1):
        '''
        Безопасно и не используя метод save увеличивает значение заданного поля на заданную величину
        '''
        model = self.__class__
        op = {field: models.F(field)+by}
        model.objects.filter(pk=self.pk).update(**op)

    @classmethod
    def get_by_identity(cls, identity, additional_filters={}):
        pk_filter = Q(slug=identity)
        try:
            pk_filter = Q(pk=int(identity))
        except:
            pass
        if 'status' in cls._meta.get_all_field_names():
            additional_filters['status'] = 'active'
        try:
            item = cls.objects.get(
                pk_filter,
                **additional_filters
            )
        except:
            raise Http404(u'Объект не найден')

        return item

    @property
    def seo_data(self):
        try:
            seo_items = list(self.seo_set.all())
        except AttributeError:
            return None
        if not seo_items:
            return None
        return seo_items[0]



class BaseModel(models.Model, CommonModelMixins):
    class Meta:
        abstract=True

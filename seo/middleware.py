# -*- coding: utf-8 -*-
import re

from hashlib import md5

from django.db.models import Q
from django.core.cache import cache
from common.shortcuts import get_object_or_none
from seo.models import Seo

class SeoMiddleware(object):
    '''
    # Добавление данных для SEO
    '''
    def process_template_response(self, request, response):
        if not response.context_data.get('seo_data'):
            path = re.sub(r'[?&]((page)|(sort))=[^&]+', '', request.get_full_path()).encode('utf8').rstrip('/')
            if not path:
                path = '/index'
            # cache_key = u'seo_page_data_%s' % md5(path).hexdigest()
            seo_page_data = False  # cache.get(cache_key)

            if not seo_page_data:
                query = Q(location=path) | Q(location='%s/' % path) | Q(location='%s*' % path)
                seo_page_data = get_object_or_none(Seo, query)
                #cache.set(cache_key, seo_page_data, 60 * 60)

            response.context_data['seo_data'] = seo_page_data
        return response
# -*- coding: utf-8 -*-

from django.contrib import admin
from django import forms
from seo.models import Seo

class SeoForm(forms.ModelForm):
    location = forms.CharField(
        required=True,
        max_length=2048,
        label="URL без домена",
        help_text= u'Например: /about'
    )
    class Meta:
        model=Seo


class SeoAdmin(admin.ModelAdmin):
    fields = ('location', 'title', 'description', 'keywords', 'visible_text')
    form = SeoForm
    def queryset(self, request):
        qs = super(SeoAdmin, self).queryset(request).filter(location__isnull=False)
        return qs

admin.site.register(Seo, SeoAdmin)
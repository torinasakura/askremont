# -*- coding: utf-8 -*-

from django.db import models
from common.models import BaseModel

class Seo(BaseModel):
    '''
    SEO - данные
    '''
    content = models.ForeignKey(
        'content.Content',
        verbose_name=u'Содержимое',
        null=True, blank=True
    )
    content_category = models.ForeignKey(
        'content.Category',
        verbose_name=u'Категория содержимого',
        null=True, blank=True
    )
    content_section = models.ForeignKey(
        'content.Section',
        verbose_name=u'Рубрика содержимого',
        null=True, blank=True
    )
    catalog_category = models.ForeignKey(
        'catalog.Category',
        verbose_name=u'Категория каталога',
        null=True, blank=True
    )
    company = models.ForeignKey(
        'catalog.Company',
        verbose_name=u'Компания',
        null=True, blank=True
    )
    location = models.CharField(
        verbose_name=u'URL без домена',
        help_text=u'Например: /contacts',
        max_length=2048,
        null=True, blank=True
    )
    title = models.CharField(
        verbose_name=u'Meta-Title',
        max_length=255,
        null=True, blank=True
    )
    keywords = models.CharField(
        verbose_name=u'Meta-Keywords',
        max_length=255,
        blank=True, null=True,
    )
    description = models.CharField(
        verbose_name=u'Meta-Description',
        max_length=512,
        blank=True, null=True,
    )
    visible_text = models.TextField(
        verbose_name=u'Видимый текст на странице',
        blank=True, null=True,
    )

    def __unicode__(self):
        return self.title if self.title else u'Запись #%s' % self.id

    class Meta:
        verbose_name = u'SEO-данные'
        verbose_name_plural = u'SEO-данные'

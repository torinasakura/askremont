# -*- coding: utf-8 -*-
from django.utils.safestring import mark_safe
from simpleblocktag import simple_block_tag
from django import template
from content.models import Content
register = template.Library()


@simple_block_tag(register)
def trans2(content):
    key = content.strip()
    #print key
    if key.startswith(u'Выберите'):
        return u'Выберите элемент из списка для редактирования'
    for k in [u'Добавить', u'Изменить']:
        if key.startswith(k):
            return u'%s элемент \'%s\'' % (k, content[9:])
    trans_map = {
        'Auth': u'Роли и пользователи',
        'Content': u'Материалы',
        'Users': u'Пользователи',
        'Market': u'Товары',
        'Django_Settings': u'Конфигурация',
        'Django_settings': u'Конфигурация',
        'Quiz': u'Анкета',
        'Catalog': u'Каталог',
        'Advert' : u'Баннерная сеть'
    }
    return trans_map.get(key, content)

@register.simple_tag()
def partial(partial_name):
    #print partial_name
    try:
        data = Content.objects.get(tech_name=partial_name, status='active')
    except Content.DoesNotExist:
        return u''

    return mark_safe(data.text)
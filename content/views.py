# -*- coding: utf-8 -*-
from django.http import Http404
from digg_paginator import DiggPaginator
from django.db.models import Q

from django.shortcuts import get_object_or_404, redirect
from django.views.generic import TemplateView, ListView
from common.shortcuts import get_object_or_none
from content.models import Content, Section, Category
from catalog.models import Company


class Index(TemplateView):
    '''
    Главная страница сайта
    '''
    template_name = 'content/index.html'

    def get_context_data(self, **kwargs):
        ctx = {
            'vertical': 'index',
            'companies': list(Company.objects.filter(status='active').order_by('?')[0:4]),
            'news': list(Content.objects.filter(status='active', category__tech_name='news').order_by('-created_at')[0:6]),
            'good_to_know': list(Content.objects.filter(status='active', category__tech_name='good_to_know').prefetch_related('related_companies').order_by('?')[0:4]),
            'articles': list(Content.objects.filter(status='active', category__tech_name='articles').order_by('-pk')[0:4]),
            'vertical': 'index',
        }
        return ctx

class ArticlesList(ListView):
    '''
    Список статей
    '''
    queryset = Content.objects.filter(status='active', category__tech_name='articles').order_by('-created_at')
    paginate_by = 15
    paginator_class = DiggPaginator
    template_name = 'content/articles_list.html'
    context_object_name = 'items'
    vertical = 'articles'

    def get_context_data(self, **kwargs):
        ctx = super(ArticlesList, self).get_context_data(**kwargs)
        base_url = '/articles/'
        seo_data = None
        if self.section:
             base_url = 'category/%s/' % self.section.identity
             seo_data = self.section.seo_data

        if not seo_data:
            cat = Category.objects.get(tech_name='articles')
            seo_data = cat.seo_data

        ctx.update({
            'vertical': self.vertical,
            'base_url': base_url,
            'sections': Section.objects.all(),
            'current_section': self.section,
            'seo_data': seo_data
        })
        return ctx

    def get_queryset(self):
        qs = super(ArticlesList, self).get_queryset()
        sec = self.kwargs.get('identity')
        self.section = None
        if sec:
            self.section = Section.get_by_identity(sec)
            qs = qs.filter(section=self.section)
        return qs


class NewsList(ListView):
    '''
    Список новостей
    '''
    queryset = Content.objects.filter(status='active', category__tech_name='news').order_by('-created_at')
    paginate_by = 15
    paginator_class = DiggPaginator
    template_name = 'content/news_list.html'
    context_object_name = 'items'
    vertical = 'news'

    def get_context_data(self, **kwargs):
        ctx = super(NewsList, self).get_context_data(**kwargs)

        cat = Category.objects.get(tech_name='news')

        ctx.update({
            'vertical': self.vertical,
            'base_url': '/news/',
            'seo_data': cat.seo_data
        })
        return ctx

class NewsView(TemplateView):
    '''
    Просмотр новости
    '''
    template_name = 'content/news_view.html'
    ctx_name = 'item'
    category_tech_name='news'

    def get_context_data(self, **kwargs):
        ctx = super(NewsView, self).get_context_data(**kwargs)
        nid = self.kwargs.get('identity')
        pk_filter = Q(slug=nid)
        try:
            pk_filter = Q(pk=int(nid))
        except:
            pass
        try:
            news_item = Content.objects.get(
                pk_filter,
                category__tech_name=self.category_tech_name,
                status='active'
            )
        except Content.DoesNotExist:
            raise Http404(u'Новость не найдена')

        prev_item = Content.objects.filter(
            category__tech_name=self.category_tech_name,
            status='active',
            created_at__lt=news_item.created_at
        ).order_by('-created_at')[0:1]

        next_item = Content.objects.filter(
            category__tech_name=self.category_tech_name,
            status='active',
            created_at__gt=news_item.created_at
        ).order_by('created_at')[0:1]

        images_gallery = [news_item] if  news_item.image else []
        images_gallery.extend(list(news_item.additionalimage_set.all()))

        news_item.inc('views_count')

        ctx.update({
            self.ctx_name: news_item,
            'prev_item': prev_item[0] if prev_item else None,
            'next_item': next_item[0] if next_item else None,
            'images_gallery': images_gallery,
            'vertical': self.category_tech_name,
            'seo_data': news_item.seo_data,
        })
        return ctx


class ArticleView(NewsView):
    template_name = 'content/article_view.html'
    ctx_name = 'item'
    category_tech_name='articles'


class StaticPage(TemplateView):
    '''
    Статические страницы
    '''

    def render_to_response(self, context, **response_kwargs):
        response_kwargs['content_type'] = self.content_type
        return super(TemplateView, self).render_to_response(context, **response_kwargs)

    def get_context_data(self, **kwargs):
        ci = self.content_item


        self.template_name = ci.template or 'content/static_page.html'
        self.content_type = ci.content_type or 'text/html'

        return {
            'content_item': ci,
            'vertical': 'static',
            'seo_data': ci.seo_data
        }

    def get(self, request, *args, **kwargs):
        slug = kwargs.get('slug').strip('/')
        self.content_item = get_object_or_none(
            Content,
            status='active',
            category__tech_name='static_page',
            slug=slug
        )

        if not self.content_item:
            return redirect('404')
        
        return super(StaticPage, self).get(request, *args, **kwargs)




class Custom404(TemplateView):
    template_name = '404.html'

    def get_context_data(self, **kwargs):
        arts = Content.objects.filter(
            status='active',
            category__tech_name='articles'
        ).order_by('?')[0:3]
        return {'articles': arts}

handler404 = Custom404.as_view()
# -*- coding: utf-8 -*-

from django.contrib import admin
from mptt.admin import MPTTModelAdmin

from content.models import (
    Category,
    Content,
    AdditionalImage,
    Section,
    Media
)

from seo.models import Seo

class SeoInline(admin.StackedInline):
    extra=0
    max_num = 1
    model=Seo
    fields = ('title', 'description', 'keywords', 'visible_text')


class CategoryAdmin(MPTTModelAdmin):
    list_display = ('title', 'tech_name')
    inlines = (SeoInline,)
admin.site.register(Category, CategoryAdmin)

from common.shortcuts import get_object_or_none


def get_content_choices(tech_name):
    cat = get_object_or_none(Category, tech_name=tech_name)
    if cat:
        return [[cat.pk, cat.title]]
    return []


class CommonContentAdmin(admin.ModelAdmin):
    def save_form(self, request, form, change):
        form.instance.category = get_object_or_none(Category, tech_name=self.tech_name)
        return super(CommonContentAdmin, self).save_form(request, form, change)

    def queryset(self, request):
        qs = super(CommonContentAdmin, self).queryset(request).filter(category__tech_name=self.tech_name)
        return qs

class AdditionalImageInline(admin.TabularInline):
    extra=0
    model = AdditionalImage


class NewsProxy(Content):
    '''
    Прокси объект для модели Материал представляющий Новости
    '''

    class Meta:
        proxy = True
        verbose_name = u'Новость'
        verbose_name_plural = u'Новости'


class NewsAdmin(CommonContentAdmin):
    prepopulated_fields = {'slug': ('title',)}
    fields = ('title', 'slug', 'status', 'image', 'announce', 'text', 'created_at')
    tech_name = 'news'
    inlines = (AdditionalImageInline, SeoInline)
admin.site.register(NewsProxy, NewsAdmin)


class ArticleProxy(Content):
    '''
    Прокси объект для модели Материал представляющий Статьи
    '''

    class Meta:
        proxy = True
        verbose_name = u'Статья'
        verbose_name_plural = u'Статьи'


class SectionAdmin(admin.ModelAdmin):
    inlines = (SeoInline,)
admin.site.register(Section, SectionAdmin)

class ArticleAdmin(CommonContentAdmin):
    prepopulated_fields = {'slug': ('title',)}
    fields = ('section', 'title', 'slug', 'status', 'image', 'announce', 'text', 'created_at')
    tech_name = 'articles'
    inlines = (AdditionalImageInline, SeoInline)
admin.site.register(ArticleProxy, ArticleAdmin)


class GoodToKnowProxy(Content):
    class Meta:
        proxy = True
        verbose_name = u'Полезно знать'
        verbose_name_plural = u'Полезно знать'

class GoodToKnowAdmin(CommonContentAdmin):
    fields = ('title','slug', 'status', 'announce', 'image', 'text', 'related_companies')
    tech_name = 'good_to_know'
    inlines = (SeoInline,)
admin.site.register(GoodToKnowProxy, GoodToKnowAdmin)


class StaticPageProxy(Content):
    class Meta:
        proxy = True
        verbose_name = u'Статическая страница'
        verbose_name_plural = u'Статические страницы'

class StaticPageAdmin(CommonContentAdmin):
    prepopulated_fields = {'slug': ('title',)}
    fields = ('title', 'status', 'slug', 'text', 'template', 'content_type')
    tech_name = 'static_page'
    inlines = (SeoInline,)
admin.site.register(StaticPageProxy, StaticPageAdmin)

class PartialProxy(Content):
    class Meta:
        proxy = True
        verbose_name = u'Вкрапление'
        verbose_name_plural = u'Вкрапления'

class PartialAdmin(CommonContentAdmin):
    fields = ('title', 'status', 'tech_name', 'text')
    tech_name = 'partial'
admin.site.register(PartialProxy, PartialAdmin)


class MediaAdmin(admin.ModelAdmin):
    list_display = ('title', 'file_url')
admin.site.register(Media, MediaAdmin)
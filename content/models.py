# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models
from common.models import BaseModel, CommonModelMixins
from mptt.models import MPTTModel, TreeForeignKey
from ckeditor.fields import RichTextField
from common.shortcuts import upload_path_constructor
from catalog.models import Company


class Category(MPTTModel, CommonModelMixins):
    parent = TreeForeignKey(
        'self',
        verbose_name = u'Родительская категория',
        null=True, blank=True,
        related_name='children'
    )
    title = models.CharField(
        verbose_name=u'Название',
        max_length=255,
    )
    tech_name = models.CharField(
        verbose_name=u'Техническое название',
        max_length=255,
        null=True, blank=True,
        help_text=u'Используется в коде для получения нужных групп данных'
    )

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Категория'
        verbose_name_plural = u'Категории'


class Section(BaseModel):
    title = models.CharField(
        verbose_name=u'Название',
        max_length=255,
    )
    slug = models.CharField(
        verbose_name=u'SEO URL',
        max_length=255,
        unique=True,
        null=True, blank=True, default=None
    )

    @property
    def identity(self):
        id = self.slug.strip()
        if not id:
            id = self.pk
        return id

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Рубрика'
        verbose_name_plural = u'Рубрики'

CONTENT_STATUS_CHOICES = (
    ('active', u'Активный'),
    ('draft', u'Черновик'),
)

CONTENT_TEMPLATE_CHOICES = (
    ('content/static_pages/no_bg.html', u'Основной шаблон'),
    ('content/static_pages/white_bg.html', u'Шаблон с белым фоном'),
    ('empty.html', u'Пустой шаблон'),
)

CONTENT_TYPE_CHOICES = (
    ('text/html', u'HTML'),
    ('text/plain', u'Текст'),

)

class Content(BaseModel):
    '''
    Контентный материал (Новость, Услуга, Акция)
    '''
    category = models.ForeignKey(
        Category,
        verbose_name=u'Категория'
    )
    section = models.ForeignKey(
        Section,
        verbose_name=u'Рубрика',
        null=True, blank=True
    )
    title = models.CharField(
        verbose_name=u'Заголовок',
        max_length=255,
    )
    slug = models.CharField(
        verbose_name=u'SEO URL',
        max_length=255,
        unique=True,
        null=True, blank=True, default=None
    )
    image = models.ImageField(
        verbose_name=u'Изображение',
        upload_to=upload_path_constructor,
        null=True, blank=True, default=None
    )
    tech_name = models.CharField(
        verbose_name=u'Техническое название',
        max_length=255,
        null=True, blank=True,
        help_text=u'Используется в шаблонах для получения нужных данных'
    )
    announce = models.TextField(
        verbose_name=u'Анонс',
        null=True, blank=True, default=None
    )
    text = RichTextField(
        verbose_name=u'Текст',
        null=True, blank=True, default=None
    )
    status = models.CharField(
        verbose_name=u'Статус',
        max_length=50,
        choices=CONTENT_STATUS_CHOICES,
        default='active',
    )
    created_at = models.DateTimeField(
        verbose_name=u'дата и время создания',
        auto_now_add=True,
    )
    created_at.editable = True

    related_companies = models.ManyToManyField(
        Company,
        verbose_name=u'Соответствующие компании',
        null=True, blank=True, default=None
    )
    views_count = models.PositiveIntegerField(
        verbose_name=u'Количество просмотров',
        editable=False,
        default=0,
    )
    template = models.CharField(
        max_length=255,
        verbose_name=u'Шаблон',
        null=True, blank=True,
        choices=CONTENT_TEMPLATE_CHOICES,
        default='content/static_page.html',
    )
    content_type = models.CharField(
        max_length=255,
        verbose_name=u'Тип содержимого',
        null=True, blank=True,
        choices=CONTENT_TYPE_CHOICES,
        default='text/html',
    )

    @property
    def link(self):
        return u'/%s/%s/' % (self.category.tech_name, self.slug or self.pk)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Материал'
        verbose_name_plural = u'Материалы'


class AdditionalImage(BaseModel):
    content = models.ForeignKey(
        Content,
        verbose_name=u'Материал'
    )
    image = models.ImageField(
        verbose_name=u'Изображение',
        upload_to=upload_path_constructor
    )

    def __unicode__(self):
        return u'Изображение #%s' % self.pk

    class Meta:
        verbose_name = u'Дополнительное изображение'
        verbose_name_plural = u'Дополнительные изображения'

class Feedback(models.Model):
    name = models.CharField(
        max_length=100,
        verbose_name=u'Имя',
    )
    text = models.TextField(
        verbose_name=u'Текст'
    )
    phone = models.CharField(
        max_length=50,
        null=True, blank=True,
    )
    email = models.EmailField(
        verbose_name=u'E-mail',
        null=True, blank=True,
    )
    created_at = models.DateTimeField(
        verbose_name=u'Дата и время добавлеия',
    )
    created_at.editable = True
    is_approved = models.BooleanField(
        verbose_name=u'Одобрен',
        default=False,
    )

    def __unicode__(self):
        return u'%s: "%s..."' % (self.name, self.text[0:40])

    class Meta:
        verbose_name = u'Отзыв'
        verbose_name_plural = u'Отзывы'


class Media(BaseModel):
    title = models.CharField(
        max_length=255,
        verbose_name=u'Назание файла',
    )
    file = models.FileField(
        verbose_name=u'Файл',
        upload_to=upload_path_constructor
    )


    def file_url(self):
        return u'%s%s' % (
            settings.MEDIA_URL,
            unicode(self.file)
        )

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Медиафайл'
        verbose_name_plural = u'Медиафайлы'

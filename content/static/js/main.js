var price_range_timeout = 0;
var stop_news_slider = false;
var news_slider_timeout = false;
var current_slider_item = 0;
function set_price_range() {
    var q = $.query
                .set('price_min', $('#price_min').val())
                .set('price_max', $('#price_max').val());
    window.location.search=q.toString();
}

function on_price_range_change() {
    clearTimeout(price_range_timeout);
    price_range_timeout = setTimeout(set_price_range, 500);
}


function switch_news(to, only_shedule) {
    var desc;
    if ($('.js-news-slider-item').length < 1) return;
    if ( ! only_shedule)
    {
        if (current_slider_item == to.attr('tgt')) return;
        var html = to.find('.js-news-slider-item').html();
        var hidden = $('.js-news-slider-main .cnt:hidden').html(html);
        $('.js-news-slider-main .cnt:visible').fadeOut();
        hidden.fadeIn();
        desc = hidden.find('.item');
    } else {
        desc = $('.js-news-slider-main .top .item');
    }
    $('.more-news li.active').removeClass('active');
    to.parent().addClass('active');
    current_slider_item = to.attr('tgt');
    desc.css({'margin-top': '-'+(desc.height()+30)+'px'});
    if ( ! stop_news_slider) {
        var next = to.parent().next();
        if ( ! next.length) {
            next = $('.more-news li:first');
        }
        news_slider_timeout = setTimeout(
            function () {
                switch_news(next.find('.js-news-slider-cont'), false)
            },
            4000
        );
    }
}

$(function() {
    switch_news($('.more-news li:first').find('.js-news-slider-cont'), true);

    $(".menu-block .item .link.js-catalog").click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        var mp = $('.menu-popup');
        if ( $(this).parent().hasClass("active") ) {
            $(this).parent().removeClass("active");
            mp.hide();
        } else {
            $(".menu-block .item").removeClass("active");
            $(this).parent().addClass("active");
            mp.show();
            if ( ! mp.hasClass('masondried')) {
                mp.addClass('masondried');
                mp.masonry({
                    columnWidth: 245,
                    itemSelector: 'div'
                });
            }
        }
    });

    $("#info-carousel").carouFredSel({
        width: 1080,
        height: 265,
        items: {
            visible: 1,
            minimum: 1,
            width: 1080,
            height: 265
        },
        scroll: {
            items: 1,
            fx: "fade",
            easing: "cubic"
        },
        auto: false,
        prev: ".info-carousel .prev",
        next: ".info-carousel .next"
    });

    $("#carousel-main").carouFredSel({
        width: 862,
        height: 267,
        items: {
            minimum: 1,
            visible: 1,
            width: 862,
            height: 267
        },
        scroll: 1,
        auto: false,
        pagination: ".carousel-main .pagination"
    });

    $('#news-slider-thumbs').carouFredSel({
        width: 490,
        height: 85,
        items: {
            width: 136
        },
        auto: false,
        prev: ".slider-cont .slide-prev",
        next: ".slider-cont .slide-next"
    });

    $('#news-slider-thumbs a').on('click', function (e) {
        e.preventDefault();
        $('.main-slide img').attr('src', $(this).attr('href'));
    });

    var price_min = $.query.get('price_min') || 0;
    var price_max = $.query.get('price_max') || 300000;

    var price_order = $.query.get('price_order') || 'desc';
    if (price_order) {
        $('#price_order').find('option').removeAttr('selected');
        $('#price_order option[value='+price_order+']').attr('selected', 'selected');
    }

    $('#price_order').on('change', function () {
        var sel_val = $('#price_order').val();
        var price_order = $.query.get('price_order') || 'desc';
        if (sel_val != price_order) {
            var q = $.query.set('price_order', sel_val);
            window.location.search=q.toString();
        }
    });

    $('#cost-slider').slider({
        min: 0,
        max: 300000,
        range: true,
        values: [price_min,price_max],
        step: 500,
        slide: function(event, ui) {
            $('.cost-min').val(ui.values[0]);
            $('.cost-max').val(ui.values[1]);
            on_price_range_change();
        }
    });

    $('.cost-min, .cost-max').on('change', function() {
        var me = $(this);
        var val = parseInt(me.val());
        if (val==NaN) return;
        var idx = me.is('.cost-min') ? 0 : 1;
        $('#cost-slider').slider("values",idx,val);
        on_price_range_change();
    });

    $('#price_min').val(price_min);
    $('#price_max').val(price_max);


    /* многабуков для того, чтобы скрывать всплывашку категорий каталога если кликнули мимо нее */
    $(".menu-popup").hover(
        function(){
            $(this).toggleClass('is-hover'); // you can use every class name you want of course
        }
    );

    $(document).on('click', function() {
        var jscat_parent = $('.menu-block .item .link.js-catalog').parent();
        if ( ! $('.menu-popup').is('.is-hover') && jscat_parent.is('.active')) {
            jscat_parent.removeClass('active');
            $('.menu-popup').hide();
        };
    });

    $('a[rel=modal]').leanModal({closeButton:'.close-modal'});

    var tim = 0;

    $('.js-news-slider-cont a').on('mouseenter', function (e) {
        e.preventDefault();
        stop_news_slider = true;
        clearTimeout(news_slider_timeout);
        var tgt = $(this).closest('.js-news-slider-cont');
        clearTimeout(tim);
        tim = setTimeout(function(){switch_news(tgt, false)}, 100)
    });

    $('.fancybox').fancybox();
})

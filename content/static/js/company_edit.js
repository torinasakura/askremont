function init_uploader(cont) {
    ASK.controllers.Uploader({
        containerId : '#' + cont.attr('id'),
        pickfilesId : '#' + cont.find('a').attr('id'),
        url : $('#add-logo-cnt').attr('data-upload-url'),
        filters : [
            {title : 'Изображения', extensions : 'jpg,png,gif'}
        ],
        isNotResultText : true,
        multipart_params : {
            csrfmiddlewaretoken : $('[name=csrfmiddlewaretoken]').val(),
            thumb_size: "382x225"
        },
        resultCallback: function (result) {
            cont.find('img').attr('src', result.thumb)
            cont.find('input').val(result.fname);
            return true;
        }
    });
}

$(function () {
    ASK.controllers.Uploader({
        containerId : '#add-logo-cnt',
        pickfilesId : '#add-logo-btn',
        url : $('#add-logo-cnt').attr('data-upload-url'),
        filters : [
            {title : 'Изображения', extensions : 'jpg,png,gif'}
        ],
        isNotResultText : true,
        multipart_params : {
            csrfmiddlewaretoken : $('[name=csrfmiddlewaretoken]').val()
        },
        resultCallback: function (result) {
            $('#logo-cont span').remove();
            $('#logo-cont img').attr('src', result.thumb)
            $('[name=logo]').val(result.fname);
            return true;
        }
    });


    $('#id_description').keyup(function () {
        var left = 540 - $(this).val().length;
        if (left < 0) {
            left = 0;
        }
        $('#desc-symbols-count').text(left);
    }).keyup();

    $.fn.aciTree.defaults.expand = true;
    $('#categories-container').aciTree({
        ajax: {
            url: $('#categories-container').attr('data-url'),
        },
        checkbox: true,
        checkboxName: 'categories',
        selectable: true
    }).on('acitree', function(event, api, item, eventName, options) {
        if (eventName == 'beforecheck') {
            var ct = $('#categories-container').find('.aciTreeChecked').length;
            var is_checked = item.is('.aciTreeChecked');
            if (ct > 2 && ! is_checked) {
                alert('Нельзя выбрать более трех категорий');
                return false;
            }

            var cat_id = api.getId(item);
            if (is_checked)  {
                $('#id_categories').find('option[value='+cat_id+']').removeAttr('selected');
            } else {
                $('#id_categories').find('option[value='+cat_id+']').attr('selected', 'selected');
            }
        }

        if (eventName == 'init') {
            api.children().each( function() {
                var me = $(this);
                if (me.find('.aciTreeChecked').length) {
                    api.open(me);
                }
            })
        }

    });


    $('.block-work .img').each(function () {
        var cont = $(this);
        if (cont.is(':visible')) {
            init_uploader(cont);
        }
    });

    $('.js-add-formset-item').on('click', function(e) {
        e.preventDefault();
        var cont = $(this).closest('.formsets');
        var last_item = cont.find('.js-formset-item:last');
        var tpl = $('<div>').append(last_item.clone()).html();
        var rex = new RegExp('_set\-([0-9])+\-', 'ig')
        var res = rex.exec(tpl)
        var next_idx = parseInt(res[1]) + 1
        tpl = tpl.replace(rex, '_set-' + next_idx + '-');
        tpl = tpl.replace(/\-([0-9])+"/ig, '-'+(next_idx+1)+'"');
        last_item.after(tpl);
        last_item.show();
        init_uploader(last_item.find('.img'));
        cont.find('[id$=TOTAL_FORMS]').val(cont.find('.js-formset-item').length - 1);
    });

    $('.js-remove-formset').on('click', function(e) {
        e.preventDefault();
        var cont=$(this).closest('.js-formset-item');
        cont.find('[id$=DELETE]').attr('checked', 'checked');
        cont.hide();
    });

    $('.serv-cost-type').on('change', function () {
        console.log('here');
        var me = $(this);
        var tgt = me.parent().find('.serv-cost-val');
        console.log(tgt, me.val());

        if (me.val() == 'fixed') {
            tgt.show();
        } else {
            tgt.find('input').val('');
            tgt.hide();
        }
    });
});
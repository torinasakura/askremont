function close_parent_modal(){
    window.parent.location='/';
}

$(function() {
    $('#login-modal-link, #reg-modal-link').on('click', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        var width = parseInt($(this).attr('mw'));
        $.fancybox({
            href           : url,
            type           : 'iframe',
            showCloseButton: true,
            padding        : 0,
            margin         : 0,
            scrolling      : 'no',
            width          : width
        });
    });


    $('#login_form').validationEngine({
        ajaxFormValidation: true,
        ajaxFormValidationMethod: 'post',
        ajaxFormValidationURL :"/auth/login/",
        onAjaxFormComplete: function(status, form, json, options)
        {
            if(status && json.false==null) // no error messages
            {
                $('#login-modal-body').html('<center>Вы успешно автоизовались!<br>Можно <a href="javascript:close_parent_modal()">приступать в к работе</a></center>');
            }
        },
        showPrompts: true,
        scroll: false
    });

    $('#registration_form').validationEngine({
        ajaxFormValidation: true,
        ajaxFormValidationMethod: 'post',
        ajaxFormValidationURL: "/auth/register/",
        onAjaxFormComplete: function(status, form, json, options)
        {
            if(status && json.false==null) // no error messages
            {
                $('.reg_toggle').toggle();
            }
        },
        showPrompts: true,
        scroll: false
    });

    $('#restore_form').validationEngine({
        ajaxFormValidation: true,
        ajaxFormValidationMethod: 'post',
        ajaxFormValidationURL :"/auth/restore/",
        onAjaxFormComplete: function(status, form, json, options)
        {
            if(status && json.false==null) // no error messages
            {
                $('#restore .restore_toggle').toggle();
            }
        },
        showPrompts: true,
        scroll: false
    });
});

$(function(){
    $('.send-block .submit').prop('disabled',true);
    $('.captcha').slider({
        min: 1,
        max: 50,
        value: 1,
        slide: function(event, ui) {
            if (ui.value>48) {
                $(this).parents('.send-block').find('.submit').prop('disabled',false);
            }
        },
        stop: function(event, ui) {
            if (ui.value>48) {
                $(this).parents('.send-block').find('.submit').prop('disabled',false);
            } else {
                $(this).find('.ui-slider-handle').animate({left:0},200);
                ui.value = 0;
            }
        }
    });

    $('.cont-btn.letter').click(function() {
        $('.first-screen').animate({left:'-700px'},400);
        $('.write-letter').animate({left:'-=740px'},400);
    });
    $('.cont-btn.call').click(function() {
        $('.first-screen').animate({left:'-700px'},400);
        $('.get-call').animate({left:'-=740px'},400);
    });
    $('.modal-back').click(function() {
        $('.first-screen').animate({left:'0'},400);
        $(this).parent().animate({left:'+=740px'},400);
    });

    $('.qaptcha').QapTcha({
      autoSubmit : false,
      autoRevert : true,
      PHPfile : '/auth/get-qaptcha/',
      submitButtonSelector: 'input[type=submit]'
    });

    $('#email_form, #phone_form').validationEngine({
        ajaxFormValidation: true,
        ajaxFormValidationMethod: 'post',
        ajaxFormValidationURL: "/auth/request/",
        onAjaxFormComplete: function(status, form, json, options)
        {
            if(status && ! json.error)
            {
                form[0].reset();
                form.parent().parent().animate({left:'-700px'},400);
                $('.contact-success').animate({left:'-=740px'},400);
                return;
            }

            if (json.msg) {
                alert(json.msg);
            }
        },
        showPrompts: true
    });
});
/**
 * ASK application
 *
 * @module: uploader
 * @requires jQuery, plupload
 */
var ASK = ASK || {};

ASK.controllers = ASK.controllers || {};



/**
 * Асинхронный загрузчик фотки
 *
 * @class: Uploader
 *
 */
ASK.controllers.Uploader = function (options) {

    'use strict';

    if (!(this instanceof ASK.controllers.Uploader)) {
        return new ASK.controllers.Uploader(options);
    }

    options = options || {};

    /***************************************************************************
     * Приватные переменные
     **************************************************************************/

    var _containerId  = options['containerId'] || '#uploader-container',
        _pickfilesId  = options['pickfilesId'] || '#uploader-pickfiles',
        _resultId     = options['resultId'] || '#uploader-result',
        _progressId   = options['progressId'] || '#uploader-progress',
        _textId       = options['textId'] || '#uploader-text',
        _isNotResultText = options['isNotResultText'],
        _resultCallback  = options['resultCallback'],

        // common
        _self       = this,
        _NS         = ASK,
        _proto      = _NS.controllers.Uploader.prototype,
        _VERSION    = '1', // версия API (смотрим какой на бекенде)
        _DEBUG      = false, // ставим в false на продакшене
        _container  = $(_containerId),
        _toInt      = function (theStr) {
                        return parseInt(theStr, 10) || 0;
                      },
        _message    = window.alert,

    	// Plupload.Uploader
    	_uploader,

    	// параметры виджета
    	_ormModelName    = _container.attr('orm-name') || '',
        _ormObjectId     = _container.attr('object-id') || 0
        ;


    // настройки plupload
    var _pluploadSettings = {
		runtimes : 'gears,html5,flash,silverlight',
		browse_button : _pickfilesId.substr(1),
        container : _containerId.substr(1),
        max_file_size : '5mb',
        url : options['url'] || '/images/upload/',
        flash_swf_url : '/static/js/vendor/plupload/backends/plupload.flash.swf',
        silverlight_xap_url : '/static/js/vendor/plupload/backends/plupload.silverlight.xap',
        filters : [
            {title : 'Image files', extensions : 'jpg,png'}
        ],
        multipart_params : options['multipart_params'] || {
			orm_name: _ormModelName,
            object_id: _ormObjectId || 0
		},
        multi_selection: false
	};



    _proto.destroy = function () {
        _uploader.unbindAll();
        _uploader.destroy();
    };



    /**
     * Меняет значение прогресс-бара
     * @param {plupload.File} file
     * @param {int} percent
     */
    var _progressBarChange = function (file, percent) {
    	percent = _toInt(percent) || file.percent;
    	var el_bar = $(_containerId + ' .bar');
    	var el_txt = $(_containerId + ' .progress-text');
    	el_bar.css('width', percent + '%');
    	el_txt.text(percent + ' %');
    	if (percent == 100 || file.percent == 100) {
    		$(_progressId).hide();
    	}
    };


    /**
     * Слушатель события добавления файла в загрузку Plupload.Uploader
     * @param up
     * @param files
     */
    var _filesAddHandler = function(up, files) {
    	var count = files.length;
    	if (count === 1) {
    		$(_textId).hide();
    		//$(_pickfilesId).hide();
    		$(_progressId).show();
    		_uploader.start();
    	}
    	else {
    		_message('Всего можно загрузить 1 фотографию. У Вас ' + count, 'error');
            up.splice();
    	}
        up.refresh(); // Reposition Flash/Silverlight
    };


    /**
     * Слушатель события прогресса загрузки файла Plupload.Uploader
     * @param up
     * @param file
     */
    var _uploadProgressHandler = function(up, file) {
    	_progressBarChange(file);
    };


    /**
     * Слушатель события окончания загрузки файла Plupload.Uploader
     * @param up
     * @param file
     * @param response - response.response,response.status
     */
    var _fileUploadedHandler = function(up, file, response) {
        var imgEl, inputEl;
        try {
            var result = JSON.parse(response.response);
            return _resultCallback(result);
        } catch (e) {
        	console.log('Ошибка загрузки: ' + e);
        	return ''
        }
        _progressBarChange(file, 100);
		$(_pickfilesId).show();
        if (!_isNotResultText) {
            $(_pickfilesId).text('Изменить');
        }
    };


    /**
     * Слушатель события обработки ошибок Plupload.Uploader
     * @param up
     * @param err
     */
    var _fileUploadErrorHandler = function(up, err) {
        var text;
        if (err.code == -601) {
            text = 'Слишком большой размер файла' +
                (err.file ? ' ' + err.file.name : '') +
                '! Размер должен быть не более ' +
                _pluploadSettings.max_file_size +
                '.';
        } else {
            text = 'Ошибка #' + err.code + ', ' + err.message;
        }
        _message(text);
        $(_textId).show();
		$(_pickfilesId).show();
		$(_progressId).hide();
		up.splice();
	    up.refresh(); // Reposition Flash/Silverlight
	};


    /**
     * Инициирует Plupload.Uploader
     */
    var _initUploader = function () {
    	_uploader = new plupload.Uploader(_pluploadSettings);
        _uploader.init();
    	_uploader.bind('FilesAdded', _filesAddHandler);
    	_uploader.bind('UploadProgress', _uploadProgressHandler);
    	_uploader.bind('Error', _fileUploadErrorHandler);
    	_uploader.bind('FileUploaded', _fileUploadedHandler);
    };



    /***************************************************************************
     * Конструктор
     **************************************************************************/
    _initUploader();

    return _self;

};

# -*- coding: utf-8 -*-

from django.forms import ModelForm
from content.models import Feedback

class FeedbackForm(ModelForm):
    class Meta:
        model=Feedback

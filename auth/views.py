# -*- coding: utf-8 -*-
from django.contrib.auth import login
from django.http import Http404
from hashlib import md5
from django.conf import settings
from django.contrib import auth
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.views.generic import RedirectView, UpdateView, TemplateView, FormView
from django.contrib.auth.models import User
from django_ulogin.signals import assign
from django.contrib import messages


import time
from auth.forms import ChangePasswordForm
from catalog.models import Company
from common.shortcuts import send_mail, get_object_or_none
from common.views.generic import JsonView, LoginRequiredMixin


class LoginModal(TemplateView):
    template_name = 'auth/_login_modal.html'

class Login(JsonView):
    def preprocess(self):
        u = get_object_or_none(User, email=self.request.POST['username'])
        username = u.username if u else ''
        password = self.request.POST['password']
        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            auth.login(self.request, user)
            return []
        return [['login_username', False, 'Неправильный логин или пароль']]


class Logout(RedirectView):
    premanent = False

    def get_redirect_url(self):
        auth.logout(self.request)
        return '/'


class Register(JsonView):
    def preprocess(self):
        email = self.request.POST['email']
        password = self.request.POST['password']
        username = email

        exists = len(User.objects.filter(Q(username=username) | Q(email=email)))
        if exists:
            return [['registration_email', False, u'Такой пользоватеть уже зарегистрирован']]
        u = User(
            email=email,
            username=username,
            is_active=True
        )
        u.save()
        u.set_password(password)
        u.save()

        greeting = render_to_string(
            'auth/success_register_email.html',
            {
                'host': self.request.META.get('HTTP_HOST'),
                'username': username,
                'password': password
            }
        )

        send_mail(
            u'Успешная регистрация на askremont',
            greeting,
            settings.EMAIL_HOST_USER,
            [email]
        )
        return []


class ChangePassword(FormView):
    template_name = 'user/change_password.html'
    form_class = ChangePasswordForm

    def form_valid(self, form):
        u = self.request.user
        u.set_password(form.cleaned_data.get('password'))
        u.save()
        return super(ChangePassword, self).form_valid(form)

    def get_success_url(self):
        messages.success(self.request, u'Пароль успешно изменен')
        return reverse('auth:change_password')

def catch_ulogin_signal(*args, **kwargs):
    """
    Обновляет модель пользователя: исправляет username, имя и фамилию на
    полученные от провайдера.
    """
    user=kwargs['user']
    json=kwargs['ulogin_data']

    if kwargs['registered']:
        user.username = json['email'].split('@')[0]
        user.email = json['email']
        user.save()

from django_ulogin.models import ULoginUser

assign.connect(receiver=catch_ulogin_signal,
               sender=ULoginUser,
               dispatch_uid='customize.models')

class Qaptcha(JsonView):
    '''
    Установка капчи
    '''

    def preprocess(self):
        ret = {'error': False}
        post = self.request.POST

        if post.get('action') and post.get('qaptcha_key'):
            if post.get('action') == 'qaptcha':
                self.request.session['qaptcha_key'] = post.get('qaptcha_key')
            else:
                self.request.session['qaptcha_key'] = False
                ret['error'] = True

        return ret

    def post(self, request, *args, **kwargs):
        return super(Qaptcha, self).get(request, *args, **kwargs)


class FeedbackRequest(JsonView):
    def preprocess(self):
        self.data = self.request.POST
        if not self.data:
            return {'error': True, 'msg': u'Пустое сообщение'}

        self.company = get_object_or_none(
            Company,
            pk=self.data.get('company_id')
        )

        if not self.company:
            return {'error': True, 'msg': u'Не найдена компания'}

        if not self.company.email:
            return {'error': True, 'msg': u'У компании не задан e-mail'}

        if 'phone' in self.data:
            return self.request_phone_feedback()
        return self.request_email_feedback()

    def request_phone_feedback(self):
        subj = u'Заявка обратного звонка для компании %s' % self.company.title
        send_mail(
            subj,
            render_to_string(
                'catalog/phone_request_email.html',
                self.data
            ),
            settings.EMAIL_HOST_USER,
            [self.company.email]
        )
        return {'error': False}


    def request_email_feedback(self):
        subj = u'Запрос на связь по email для компании %s' % self.company.title
        send_mail(
            subj,
            render_to_string(
                'catalog/email_request_email.html',
                self.data
            ),
            settings.EMAIL_HOST_USER,
            [self.company.email]
        )
        return {'error': False}

class DoRestore(RedirectView):
    def get_redirect_url(self, **kwargs):

        # Восстановление возможно только в течение суток
        t = int(kwargs.get('timestamp'))
        if t < int(time.time()) - 24*60*60:
            raise Http404

        # Проверяем есть ли такой пользователь
        uid = int(kwargs.get('user_id'))
        u = get_object_or_404(User, pk=uid)


        # Проверяем валидность данных
        hash_to_check = kwargs.get('hash')

        hash_str = '%s%s%s' % (t, u.pk, settings.AUTH_SALT)
        hash_real = md5(hash_str).hexdigest()

        if hash_to_check != hash_real:
            raise Http404

        # Авторизовываем пользователя и отправлем менять пароль
        u.backend = 'django.contrib.auth.backends.ModelBackend'
        login(self.request, u)

        return reverse('auth:change_password')



class RestorePassword(JsonView):
    def preprocess(self):
        u = get_object_or_none(User, email=self.request.POST.get('email'))
        if not u:
            return [['restore_email', False, u'Пользователь не найден']]

        subj = u'Восстановление пароля на askremont.ru'
        t = int(time.time())
        hash_str = '%s%s%s' % (t, u.pk, settings.AUTH_SALT)
        url_kwargs = {
            'user_id': u.pk,
            'timestamp': t,
            'hash': md5(hash_str).hexdigest()
        }

        url = reverse('auth:do_restore_password', kwargs=url_kwargs)

        url = 'http://%s%s' % (self.request.META.get('HTTP_HOST'), url)
        send_mail(
            subj,
            render_to_string(
                'auth/restore_password_email.html',
                {
                    'restore_url': url
                }
            ),
            settings.EMAIL_HOST_USER,
            [u.email]
        )
        return []
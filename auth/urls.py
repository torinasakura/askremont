# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from django.views.decorators.csrf import csrf_exempt as ce
from django.views.generic import TemplateView
from auth.views import (
    LoginModal,
    Login,
    Logout,
    Register,
    ChangePassword,
    Qaptcha,
    FeedbackRequest,
    DoRestore,
    RestorePassword
)

urlpatterns = patterns('',
    url(r'^login-modal/$', LoginModal.as_view(), name='login_modal'),
    url(
        r'^restore-password/$',
        TemplateView.as_view(template_name='auth/_restore_password.html'),
        name='restore_password_page'
    ),
    url(
        r'^register-modal/$',
        TemplateView.as_view(template_name='auth/_register_modal.html'),
        name='register_modal'
    ),
    url(r'^login/$', Login.as_view(), name='login'),
    url(r'^logout/$', Logout.as_view(), name='logout'),
    url(r'^register/$', Register.as_view(), name='register'),
    url(r'^change-password/$', ChangePassword.as_view(), name='change_password'),
    url(r'^get-qaptcha/$', ce(Qaptcha.as_view()), name='get_qaptcha'),
    url(r'^request/$', ce(FeedbackRequest.as_view()), name='feedback_request'),
    url(r'^restore/$', ce(RestorePassword.as_view()), name='restore_password'),
    url(r'^dorestore/(?P<hash>[0-9a-f]+)/(?P<timestamp>[0-9]+)/(?P<user_id>[0-9]+)/$', DoRestore.as_view(), name='do_restore_password'),
)
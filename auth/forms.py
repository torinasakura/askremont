# -*- coding: utf-8 -*-
from django import forms
from django.core.exceptions import ValidationError


class ChangePasswordForm(forms.Form):
    msg = u''
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    def clean_confirm_password(self):
        ctx = self.cleaned_data
        p = ctx.get('password')
        cp = ctx.get('confirm_password')
        if p and cp:
            if p != cp:
                raise ValidationError(u'Пароли не совпадают')